%%%%
%%%% Unit tests for macro `case`
%%%%

b2i : Bool -> Int;
b2i a = case a
  | true => 1
  | _ => 0;

last : List Int -> Option Int;
last xs = case xs
  | cons x nil => some x
  | cons x xs => last xs
  | _ => none;

test-one-var = do {
  Test_info "MACRO CASE" "one variable";

  r0 <- Test_eq "b2i" (b2i true) 1;
  r1 <- Test_eq "b2i" (b2i false) 0;

  r2 <- Test_eq "last" (last nil) none;
  r3 <- Test_eq "last" (last (cons 1 nil)) (some 1);
  r4 <- Test_eq "last" (last (cons 1 (cons 2 nil))) (some 2);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 r4))));

  if success then
    (Test_info "MACRO CASE" "test on one variable succeeded")
  else
    (Test_warning "MACRO CASE" "test on one variable failed");

  IO_return success;
};

dflt1 : Bool -> Bool -> Bool -> Int;
dflt1 a b c = case (a,b,c)
  | (true,false,_) => 1
  | (false,_,true) => 2
  | (_,true,false) => 3
  | (true,true,true) => 4
  | (_,_,_) => 0;

dflt2 : Bool -> Bool -> Bool -> Int;
dflt2 a b c = case (a,b,c)
  | (true,true,true) => 4
  | (_,true,false) => 3
  | (false,_,true) => 2
  | (true,false,_) => 1
  | (_,_,_) => 0;

dflt3 : Bool -> Bool -> Bool -> Int;
dflt3 a b c = case (a,b,c)
  | (_,_,true) => 777
  | (_,false,false) => 888
  | (_,_,_) => 999;

nand : Bool -> Bool -> Bool;
nand b0 b1 = case (b0,b1)
  | (true,true) => false
  | _ => true;

test-dflt = do {
  Test_info "MACRO CASE" "default";

  r0 <- Test_eq "dflt1" (dflt1 true true true) 4;
  r1 <- Test_eq "dflt1" (dflt1 true true false) 3;
  r2 <- Test_eq "dflt1" (dflt1 true false true) 1;
  r3 <- Test_eq "dflt1" (dflt1 true false false) 1;
  r4 <- Test_eq "dflt1" (dflt1 false true true) 2;
  r5 <- Test_eq "dflt1" (dflt1 false true false) 3;
  r6 <- Test_eq "dflt1" (dflt1 false false true) 2;
  r7 <- Test_eq "dflt1" (dflt1 false false false) 0;

  r8 <- Test_eq "dflt2" (dflt1 true true true) 4;
  r9 <- Test_eq "dflt2" (dflt1 true true false) 3;
  r10 <- Test_eq "dflt2" (dflt1 true false true) 1;
  r11 <- Test_eq "dflt2" (dflt1 true false false) 1;
  r12 <- Test_eq "dflt2" (dflt1 false true true) 2;
  r13 <- Test_eq "dflt2" (dflt1 false true false) 3;
  r14 <- Test_eq "dflt2" (dflt1 false false true) 2;
  r15 <- Test_eq "dflt2" (dflt1 false false false) 0;

  r16 <- Test_false "nand" (nand true true);
  r17 <- Test_true "nand" (nand true false);
  r18 <- Test_true "nand" (nand false true);
  r19 <- Test_true "nand" (nand false false);

  r20 <- Test_eq "dflt3" (dflt3 true false false) 888;
  r21 <- Test_eq "dflt3" (dflt3 true true false) 999;
  r22 <- Test_eq "dflt3" (dflt3 true false true) 777;

  part1 <- IO_return (and r0 (and r1 (and r2 (and r3
            (and r4 (and r5 (and r6 r7)))))));

  part2 <- IO_return (and r8 (and r9 (and r10 (and r11
            (and r12 (and r13 (and r14 r15)))))));

  part3 <- IO_return (and r16 (and r17 (and r18 r19)));

  part4 <- IO_return (and r20 (and r21 r22));

  success <- IO_return (and part1 (and part2 (and part3 part4)));

  if success then
    (Test_info "MACRO CASE" "test on default succeeded")
  else
    (Test_warning "MACRO CASE" "test on default failed");

  IO_return success;
};

sum : List Int -> List Int -> Int;
sum xs ys = case (xs,ys)
  | (cons x xs,cons y ys) => (x + y) + (sum xs ys)
  | (_,_) => 0;

rm-last : List Int -> List Int;
rm-last xs = case xs
  | cons x nil => (nil : List Int)
  | cons x xs => cons x (rm-last xs)
  | _ => (nil : List Int);

test-list = do {
  Test_info "MACRO CASE" "list";

  r0 <- Test_eq "sum" (sum nil nil) 0;
  r1 <- Test_eq "sum" (sum (cons 1 nil) (cons 1 nil)) 2;
  r2 <- Test_eq "sum" (sum (cons 1 (cons 100 nil)) (cons 1 nil)) 2;

  r3 <- Test_eq "rm-last" (rm-last nil) nil;
  r4 <- Test_eq "rm-last" (rm-last (cons 1 nil)) nil;
  r5 <- Test_eq "rm-last" (rm-last (cons 1 (cons 2 nil))) (cons 1 nil);

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 r5)))));

  if success then
    (Test_info "MACRO CASE" "test on list succeeded")
  else
    (Test_warning "MACRO CASE" "test on list failed");

  IO_return success;
};

type APColor
  | ap-red
  | ap-green
  | ap-blue;

type SPColor
  | sp-magenta
  | sp-cyan
  | sp-yellow;

type Color
  | red
  | magenta
  | blue
  | cyan
  | green
  | yellow
  | white
  | black;

mix-add : APColor -> APColor -> Color;
mix-add a b = case (a, b)
  | (ap-red, ap-red) => red
  | (ap-blue, ap-blue) => blue
  | (ap-green, ap-green) => green
  | (ap-red, ap-blue) => magenta
  | (ap-blue, ap-red) => magenta
  | (ap-blue, ap-green) => cyan
  | (ap-green, ap-blue) => cyan
  | (ap-green, ap-red) => yellow
  | (ap-red, ap-green) => yellow;

mix-sub : SPColor -> SPColor -> Color;
mix-sub a b = case (a, b)
  | (sp-magenta, sp-magenta) => magenta
  | (sp-cyan, sp-cyan) => cyan
  | (sp-yellow, sp-yellow) => yellow
  | (sp-magenta, sp-cyan) => blue
  | (sp-cyan, sp-magenta) => blue
  | (sp-cyan, sp-yellow) => green
  | (sp-yellow, sp-cyan) => green
  | (sp-yellow, sp-magenta) => red
  | (sp-magenta, sp-yellow) => red;

to-ap : Color -> Option APColor;
to-ap c = case c
  | red => some ap-red
  | green => some ap-green
  | blue => some ap-blue
  | _ => none;

to-sp : Color -> Option SPColor;
to-sp c = case c
  | magenta => some sp-magenta
  | cyan => some sp-cyan
  | yellow => some sp-yellow
  | _ => none;

test-color = do {
  Test_info "MACRO CASE" "color";

  r0 <- Test_eq "mix-add" (mix-add ap-red ap-red) red;
  r1 <- Test_eq "mix-add" (mix-add ap-blue ap-blue) blue;
  r2 <- Test_eq "mix-add" (mix-add ap-green ap-green) green;
  r3 <- Test_eq "mix-add" (mix-add ap-red ap-blue) magenta;
  r4 <- Test_eq "mix-add" (mix-add ap-blue ap-red) magenta;
  r5 <- Test_eq "mix-add" (mix-add ap-blue ap-green) cyan;
  r6 <- Test_eq "mix-add" (mix-add ap-green ap-blue) cyan;
  r7 <- Test_eq "mix-add" (mix-add ap-green ap-red) yellow;
  r8 <- Test_eq "mix-add" (mix-add ap-red ap-green) yellow;

  r9 <- Test_eq "mix-sub" (mix-sub sp-magenta sp-magenta) magenta;
  r10 <- Test_eq "mix-sub" (mix-sub sp-cyan sp-cyan) cyan;
  r11 <- Test_eq "mix-sub" (mix-sub sp-yellow sp-yellow) yellow;
  r12 <- Test_eq "mix-sub" (mix-sub sp-magenta sp-cyan) blue;
  r13 <- Test_eq "mix-sub" (mix-sub sp-cyan sp-magenta) blue;
  r14 <- Test_eq "mix-sub" (mix-sub sp-cyan sp-yellow) green;
  r15 <- Test_eq "mix-sub" (mix-sub sp-yellow sp-cyan) green;
  r16 <- Test_eq "mix-sub" (mix-sub sp-yellow sp-magenta) red;
  r17 <- Test_eq "mix-sub" (mix-sub sp-magenta sp-yellow) red;

  part1 <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4
                     (and r5 (and r6 (and r7 r8))))))));

  part2 <- IO_return (and r9 (and r10 (and r11 (and r12 (and r13
                     (and r14 (and r15 (and r16 r17))))))));

  success <- IO_return (and part1 part2);

  if success then
    (Test_info "MACRO CASE" "test on color succeeded")
  else
    (Test_warning "MACRO CASE" "test on color failed");

  IO_return success;
};

f1 : List (Option Bool) -> List (Option Bool) -> Int;
f1 xs ys = case (xs,ys)
  | (cons (some true) xs, cons (some true) ys) => f1 xs ys
  | (cons none nil, cons none nil) => 2
  | (nil,nil) => 1
  | (_,_) => 0;

f3 : Option Bool -> Option Bool -> Int;
f3 ob0 ob1 = case (ob0,ob1)
  | (some true, some true) => 0
  | (some false, some true) => 1
  | (some true, some false) => 2
  | (some false, some false) => 3
  | (some true, none) => 4
  | (some false, none) => 5
  | (none, some true) => 6
  | (none, some false) => 7
  | (none, none) => 8;

test-sub-pattern = do {
  Test_info "MACRO CASE" "sub pattern";

  r0 <- Test_eq "f1" (f1 (cons (some true) nil) (cons (some true) nil)) 1;
  r1 <- Test_eq "f1" (f1 (cons (some true) (cons (some true) nil))
                         (cons (some true) (cons (some true) nil))) 1;
  r2 <- Test_eq "f1" (f1 nil nil) 1;
  r3 <- Test_eq "f1" (f1 (cons (some true) (cons none nil)) (cons (some true) (cons none nil))) 2;
  r4 <- Test_eq "f1" (f1 (cons (some false) nil) (cons (some false) nil)) 0;
  r5 <- Test_eq "f1" (f1 (cons none nil) (cons (some true) nil)) 0;
  r6 <- Test_eq "f1" (f1 (cons (some true) nil) (cons none nil)) 0;
  r7 <- Test_eq "f1" (f1 (cons (none : Option Bool) nil) (cons (none : Option Bool) nil)) 2;

  r8 <- Test_eq "f3" (f3 (some true) (some true)) 0;
  r9 <- Test_eq "f3" (f3 (some false) (some true)) 1;
  r10 <- Test_eq "f3" (f3 (some true) (some false)) 2;
  r11 <- Test_eq "f3" (f3 (some false) (some false)) 3;
  r12 <- Test_eq "f3" (f3 (some true) none) 4;
  r13 <- Test_eq "f3" (f3 (some false) none) 5;
  r14 <- Test_eq "f3" (f3 none (some true)) 6;
  r15 <- Test_eq "f3" (f3 none (some false)) 7;
  r16 <- Test_eq "f3" (f3 none none) 8;

  part1 <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4
                     (and r5 (and r6 r7)))))));

  part2 <- IO_return (and r8 (and r9 (and r10 (and r11 (and r12
                     (and r13 (and r14 (and r15 r16))))))));

  success <- IO_return (and part1 part2);

  if success then
    (Test_info "MACRO CASE" "test on sub pattern succeeded")
  else
    (Test_warning "MACRO CASE" "test on sub pattern failed");

  IO_return success;
};

%% `cons x xs` vs `cons a as` and `cons y ys` vs `cons b bs`

f5 : List Bool -> List Bool -> Int;
f5 xs ys = let

  count : Bool -> Bool -> Int;
  count a b = case (a, b)
    | (true,true) => 2
    | (true,false) => 1
    | (false,true) => 1
    | (false,false) => 0;

in case (xs, ys)
  | (cons x xs, nil) => (count x false) + (f5 xs nil)
  | (nil, cons y ys) => (count false y) + (f5 nil ys)
  | (cons a as, cons b bs) => (count a b) + (f5 as bs)
  | (nil, nil) => 0;

test-var-name = do {
  Test_info "MACRO CASE" "variable names";

  r0 <- Test_eq "f5" (f5 (cons true nil) nil) 1;
  r1 <- Test_eq "f5" (f5 nil (cons false nil)) 0;
  r2 <- Test_eq "f5" (f5 (cons true nil) (cons true nil)) 2;
  r3 <- Test_eq "f5" (f5 (cons false nil) (cons false nil)) 0;
  r4 <- Test_eq "f5" (f5 (cons true nil) (cons false nil)) 1;
  r5 <- Test_eq "f5" (f5 nil nil) 0;
  r6 <- Test_eq "f5" (f5 (cons true (cons false nil)) (cons true (cons true nil))) 3;

  success <- IO_return (and r0 (and r1 (and r2 (and r3 (and r4 (and r5 r6))))));

  if success then
    (Test_info "MACRO CASE" "test on variable names succeeded")
  else
    (Test_warning "MACRO CASE" "test on variable names failed");

  IO_return success;
};

type AllImplicit
  | ctor2 (a :: Int) (b :: Bool) (c :: Option Bool);

f6 : AllImplicit -> Bool;
f6 c = case c
  | ctor2 (_ := va) (_ := vb) (_ := vc) => vb;

f7 : AllImplicit -> Bool;
f7 c = case c
  | ctor2 (c := vc) (a := va) (b := vb) => vb;

test-named = do {
  Test_info "MACRO CASE" "named argument";

  r0 <- Test_false "default" (f6 (ctor2 (a := 0) (b := false) (c := none)));
  r1 <- Test_true  "default" (f6 (ctor2 (a := 0) (b := true) (c := none)));
  r2 <- Test_false "order"   (f7 (ctor2 (a := 0) (b := false) (c := none)));
  r3 <- Test_true  "order"   (f7 (ctor2 (a := 0) (b := true) (c := none)));

  success <- IO_return (and r0 (and r1 (and r2 r3)));

  if success then
    (Test_info "MACRO CASE" "test on named argument succeeded")
  else
    (Test_warning "MACRO CASE" "test on named argument failed");

  IO_return success;
};

exec-test = do {
  b1 <- test-one-var;
  b2 <- test-dflt;
  b3 <- test-list;
  b4 <- test-color;
  b5 <- test-sub-pattern;
  b6 <- test-var-name;
  b7 <- test-named;

  IO_return (and b1 (and b2 (and b3 (and b4 (and b5 (and b6 b7))))));
};
