(* Copyright (C) 2020  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Env
open Lexp
open Pexp

type location = Util.location

let error ~(location : location) (message : string) : 'a =
  Log.log_fatal ~section:"HEAP" ~loc:location message

let dloc = Util.dummy_location
let type0 = Debruijn.type0
let type_datacons_label = mkBuiltin ((dloc, "DataconsLabel"), type0)

let datacons_label_of_string location depth = function
  | [Vstring _ as label] -> label
  | _ -> error ~location "`##datacons-label<-string` expects 1 string argument"

let register_builtins () =
  Builtin.add_builtin_cst "DataconsLabel" type_datacons_label;
  Eval.add_builtin_function "datacons-label<-string" datacons_label_of_string 1
