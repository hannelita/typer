(* unify_test.ml --- Test the unification algorithm
 *
 *      Copyright (C) 2016-2020  Free Software Foundation, Inc.
 *
 *   Author: Vincent Bonnevalle <tiv.crb@gmail.com>
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- *)


open Lexp
open Unification

open Fmt
open Utest_lib

module U = Util

(* default environment *)
let ectx = Elab.default_ectx
let rctx = Elab.default_rctx

type result =
  | Constraint
  | Unification
  | Equivalent
  | Nothing

let string_of_result r =
  match r with
  | Constraint  -> "Constraint"
  | Unification -> "Unification"
  | Equivalent  -> "Equivalent"
  | Nothing     -> "Nothing"

let unif_output (lxp1: lexp) (lxp2: lexp) ctx =
  let orig_subst = !metavar_table in
  let constraints = unify lxp1 lxp2 ctx in
  match constraints with
  | []
    -> let new_subst = !metavar_table in
      if orig_subst == new_subst
      then (Equivalent, constraints)
      else (Unification, constraints)
  | (CKresidual, _, _, _)::_      -> (Constraint, constraints)
  | (CKimpossible, _, _, _)::_    -> (Nothing, constraints)

let add_unif_test name ?(ectx=ectx) lxp_a lxp_b expected =
  add_test "UNIFICATION" name (fun () ->
      let (r, cstrts) = unif_output lxp_a lxp_b (DB.ectx_to_lctx ectx) in

      if r = expected then
        success
      else (
        ut_string2 (red ^ "EXPECTED: " ^ reset ^ (string_of_result expected) ^ "\n");
        ut_string2 (red ^ "GOT:      " ^ reset ^ (string_of_result r       ) ^ "\n");
        ut_string2 ("During the unification of:\n\t" ^ (lexp_string lxp_a)
                    ^ "\nand\n\t" ^ (lexp_string lxp_b) ^ "\n");
        failure
      ))

let add_unif_test_s name ?(setup="") ?(ectx=ectx) input_a input_b expected =
  let _, ectx = Elab.lexp_decl_str setup ectx in

  let lxp_a = List.hd (Elab.lexp_expr_str input_a ectx) in
  let lxp_b = List.hd (Elab.lexp_expr_str input_b ectx) in

  add_unif_test name ~ectx lxp_a lxp_b expected

let _ =
  (* Let's have some variables in context to block the reduction of
     elimination forms. The variables are manually added to the
     context (and not given a value) to make sure that they cannot be
     reduced. *)
  let _, ectx = Elab.lexp_decl_str
                  {| type Nat
                     | Z
                     | S (Nat); |} ectx in
  let dloc = U.dummy_location in
  let nat = mkVar ((dloc, Some "Nat"), 2) in
  let shift l i = mkSusp l (S.shift i) in
  let ectx, _ =
    List.fold_left
      (fun (ectx, i) (name, lexp) ->
        Elab.ctx_extend ectx (dloc, Some name) Variable (shift lexp i), i + 1)
      (ectx, 0)
      [("f", (mkArrow (Anormal, (dloc, None), nat, dloc, shift nat 1)));
       ("g", (mkArrow (Anormal, (dloc, None), nat, dloc, shift nat 1)));
       ("h", (mkArrow (Anormal, (dloc, Some "x"), nat, dloc,
                       mkArrow (Anormal, (dloc, Some "y"), shift nat 1,
                                dloc, shift nat 2))));
       ("a", nat);
       ("b", nat)] in

  add_unif_test_s "same integer"   "4" "4" Equivalent;
  add_unif_test_s "diff. integers" "3" "4" Nothing;
  add_unif_test_s "int and builtin" "3" "##Int" Nothing;
  add_unif_test_s "same var" ~ectx "a" "a" Equivalent;
  add_unif_test_s "diff. var" ~ectx "a" "b" Constraint;
  add_unif_test_s "var and integer" ~ectx "a" "1" Constraint;
  add_unif_test_s "same call" ~ectx "f a"  "f a" Equivalent;
  add_unif_test_s "calls with inconvertible heads" ~ectx "f a"  "g a" Constraint;
  add_unif_test_s "calls with diff. num. of args" ~ectx "h a"  "h a b" Constraint;
  add_unif_test_s "calls with residue in args" ~ectx "f a"  "f b" Constraint;
  add_unif_test_s "same case" ~ectx
    "case a | Z => false | S n => true"
    "case a | Z => false | S n => true"
    Equivalent;
  add_unif_test_s "diff. case" ~ectx
    "case a | Z => false | S n => true"
    "case a | Z => true | S n => false"
    (* 'Nothing' would be a more accurate result here. This would
       require implementing unification for case exprs. *)
    Constraint;

  add_unif_test_s "datacons/inductive" ~ectx
    "Z"
    "(datacons (typecons Nat Z (S Nat)) Z)"
    (* Not recursive! Refers to the previous def of Nat. *)
    Equivalent;

  (* Metavariables *)
  add_unif_test_s "same metavar" "?m" "?m" Equivalent;
  add_unif_test_s "diff. metavar" "?m1" "?m2" Unification;
  add_unif_test_s "metavar and int" "?m" "5" Unification;

  ()

let _ = run_all ()
